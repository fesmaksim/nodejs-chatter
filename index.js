var express = require('express');
var app = express();
var http = require('http');
var fs = require('fs');
var pg = require('pg');
var ajaxRequest = require('request');
var mongoHelper = require('helpers/mongo');
// var MongoClient = require('mongodb').MongoClient;
// var ObjectId = require('mongodb').ObjectID;
// var assert = require('assert');



app.set('port', (process.env.PORT || 5000));

app.use('/styles', express.static(__dirname + '/styles'));

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


//----------------------------------------
// REDIRECT TO PAGES
//----------------------------------------
app.get('/', function(request, response) {
	fs.readFile('views/pages/index.html', function (err, html) {
		if (err) {
			throw err;
		}

		response.writeHeader(200, {"Content-Type": "text/html"});
		response.write(html);
		response.end();
	});
});

app.get('/home', function(request, response) {
	fs.readFile('views/pages/home.html', function (err, html) {
		if (err) {
			throw err;
		}

		response.writeHeader(200, {"Content-Type": "text/html"});
		response.write(html);
		response.end();
	});
});

//----------------------------------------
// HANDLE AJAX
//----------------------------------------
app.get('/singin', function(request, response) {
	console.log('params: ' + JSON.stringify(request.params));
	console.log('body: ' + JSON.stringify(request.body));
	console.log('query: ' + JSON.stringify(request.query));

	var responseBody = mongoHelper.retrieve('users', query, '{"_id":1}');

	if (responseBody != undefined) {//Such user exists already
		response.writeHeader(200, {"Content-Type": "application/json"});
		response.write(responseBody);
		response.end();
	} else {
		response.writeHeader(400, {"Content-Type": "application/json"});
		response.write();
		response.end();
	}
});

app.get('/singup', function(request, response) {

	console.log('params: ' + JSON.stringify(request.params));
	console.log('body: ' + JSON.stringify(request.body));
	console.log('query: ' + JSON.stringify(request.query));

	var query = request.query;

	var responseBody = mongoHelper.retrieve('users', query, '{"_id":1}');

	if (responseBody != undefined) {//Such user exists already
		response.writeHeader(400, {"Content-Type": "application/json"});
		response.write({
			status_code: 400,
			message: 'Try to use another username.'
		});
		response.end();
	} else {//insert a new user
		response.writeHeader(200, {"Content-Type": "application/json"});
		response.write(mongoHelper.insert('users', query));
		response.end();
	}
});











